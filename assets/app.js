import "./bootstrap.js";
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import "./styles/app.css";

import "./vendor/preline/dist/preline.js";

import "./vendor/aos/dist/aos.css";

import AOS from "aos";

AOS.init({
  duration: 1000,
});


window.axeptioSettings = {
  clientId: "6660532a56a33280b6ef50ff",
  };
  
  (function(d, s) {
  var t = d.getElementsByTagName(s)[0], e = d.createElement(s);
  e.async = true; e.src = "//static.axept.io/sdk.js";
  t.parentNode.insertBefore(e, t);
})(document, "script");

