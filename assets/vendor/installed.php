<?php return array (
  '@hotwired/stimulus' => 
  array (
    'version' => '3.2.2',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  '@hotwired/turbo' => 
  array (
    'version' => '7.3.0',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'preline' => 
  array (
    'version' => '2.1.0',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  '@tailwindcss/forms' => 
  array (
    'version' => '0.5.7',
    'dependencies' => 
    array (
      0 => 'mini-svg-data-uri',
      1 => 'tailwindcss/plugin',
      2 => 'tailwindcss/defaultTheme',
      3 => 'tailwindcss/colors',
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'mini-svg-data-uri' => 
  array (
    'version' => '1.4.4',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'tailwindcss/plugin' => 
  array (
    'version' => '3.3.5',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'tailwindcss/defaultTheme' => 
  array (
    'version' => '3.3.5',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'tailwindcss/colors' => 
  array (
    'version' => '3.3.5',
    'dependencies' => 
    array (
      0 => 'picocolors',
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'picocolors' => 
  array (
    'version' => '1.0.0',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'aos' => 
  array (
    'version' => '2.3.4',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
  'typed.js' => 
  array (
    'version' => '2.1.0',
    'dependencies' => 
    array (
    ),
    'extraFiles' => 
    array (
    ),
  ),
);