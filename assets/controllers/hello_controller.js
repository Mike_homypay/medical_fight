import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
    connect() {
        window.addEventListener('scroll', () => {
            const verticalScrollPx = window.scrollY;
          
            if (verticalScrollPx > 500) {
                document.querySelector('#navbar').classList.add('bg-zinc-600');
            } else {document.querySelector('#navbar').classList.remove('bg-zinc-600');}
          });
          
    }
}
