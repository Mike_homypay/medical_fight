<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('objet', TextType::class, [
                'constraints' => [
                    new Length(
                        [
                            "min" => 2,
                            "max" => 50
                        ]
                    ),
                    new NotBlank(
                        [
                            "message" => "Veuillez renseigner l'objet de votre message"
                        ]
                    ),
                ],
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Laissez-nous un message...',
                'constraints' => [
                    new Length(
                        [
                            "min" => 10,
                            "max" => 500,
                            "minMessage" => "Votre message doit contenir au moins {{ limit }} caractères.",
                            "maxMessage" => "Votre message doit contenir au plus {{ limit }} caractères.",
                        ]
                    ),
                    new NotBlank(
                        [
                            "message" => "Veuillez renseigner votre message"
                        ]
                    ),
                ]
            ])
            ->add('nom', TextType::class, [
                'constraints' => [
                    new Length(
                        [
                            "min" => 2,
                            "max" => 30,
                            "minMessage" => "Votre nom doit contenir au moins {{ limit }} caractères.",
                            "maxMessage" => "Votre nom doit contenir au plus {{ limit }} caractères.",
                        ]
                    ),
                    new NotBlank(
                        [
                            "message" => "Veuillez renseigner votre nom"
                        ]
                    ),
                ],
            ])
            ->add('prenom', TextType::class, [
                'constraints' => [
                    new Length(
                        [
                            "min" => 2,
                            "max" => 30,
                            "minMessage" => "Votre prénom doit contenir au moins {{ limit }} caractères.",
                            "maxMessage" => "Votre prénom doit contenir au plus {{ limit }} caractères.",
                        ]
                    ),
                    new NotBlank(
                        [
                            "message" => "Veuillez renseigner votre prénom"
                        ]
                    ),

                ],
            ])
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner votre email',
                    ]),
                ],
            ])
            ->add('captcha', Recaptcha3Type::class, [
                'constraints' => new Recaptcha3(),
                'action_name' => 'commentaire',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
