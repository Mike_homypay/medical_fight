<?php

namespace App\Form;

use App\Entity\Commentaires;
use Symfony\Component\Form\AbstractType;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;

class CommentairesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('pseudo', TextType::class, [
                'label' => 'Pseudo',
                'help' => 'Votre pseudo doit contenir entre 2 et 30 caractères.',
                'help_attr' => [
                    'class' => 'text-xs text-amber-500'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer votre pseudo.'
                    ]),
                    new Length([
                        'min' => 2,
                        'max' => 30,
                    ])
                ]
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Message',
                'help' => 'Votre message doit contenir entre 10 et 500 caractères.',
                'help_attr' => [
                    'class' => 'text-xs text-amber-500'
                ],
                
                'constraints' => [
                    new Length([
                        'min' => 10,
                       'minMessage' => 'Votre message doit contenir au moins {{ limit }} caractères.',
                       'max' => 500,
                    ]),
                    new NotBlank([
                        'message' => 'Veuillez entrer un message.'
                    ])
                ]
            ])
            ->add('note', IntegerType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer une note entre 1 et 5.'
                    ])
                ]
            ])
            ->add('titre', TextType::class, [
                'label' => 'Titre',
                'required' => true,
                'help' => 'Votre titre doit contenir entre 2 et 30 caractères.',
                'help_attr' => [
                    'class' => 'text-xs text-amber-500'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer votre pseudo.'
                    ]),
                    new Length([
                        'min' => 2,
                        'max' => 30,
                    ])
                ]
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'w-full py-3 px-4 block items-center gap-x-2 text-sm  rounded-lg border border-transparent bg-gradient-to-tr from-amber-400 to-amber-600 text-zinc-800 hover:bg-grandient-to-l hover:from-zinc-800 hover:text-amber-200 font-bold disabled:opacity-50 disabled:pointer-events-none dark:bg-white dark:text-neutral-800',
                ]
            ])
            ->add('captcha', Recaptcha3Type::class, [
                'constraints' => new Recaptcha3(),
                'action_name' => 'commentaire',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Commentaires::class,
        ]);
    }
}
