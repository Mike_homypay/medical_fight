<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Repository\CommentairesRepository;
use App\Service\FormService;
use App\Service\NavbarButtonsService;
use App\Service\RecuperationDeDonneesService;
use Egulias\EmailValidator\Parser\Comment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonneesService $recuperationDeDonnees,
        private NavbarButtonsService $navbarButtonsService,
    ) {
    }
    #[Route('/', name: 'app_home')]
    public function index(CommentairesRepository $commentairesRepository): Response
    {
        $experience = $this->recuperationDeDonnees->experienceCardItem();
        $temoignages = $this->recuperationDeDonnees->experience();
        $mf_proposition_organisations = $this->navbarButtonsService->mf_proposition_organisation();
        $formule_cle_en_main = $this->navbarButtonsService->formule_cle_en_main();
        $commentaires = $commentairesRepository->findBy(['isValid' => true]);
        $partenaires = $this->recuperationDeDonnees->partenaires();

        $nb_commentaires = count($commentaires);
        $somme = 0;
        foreach ($commentaires as $commentaire) {
            $somme += $commentaire->getNote();
        }

        if ($nb_commentaires != 0) {
            $note_globale = $somme / $nb_commentaires;
            $nb_stars = round($note_globale);
        } else {
            $note_globale = 1;
            $nb_stars = 1;
        }

        return $this->render('home/index.html.twig', [
            'experience' => $experience,
            'temoignages' => $temoignages,
            'mf_proposition_organisations' => $mf_proposition_organisations,
            'formule_cle_en_main' => $formule_cle_en_main,
            'commentaires' => $commentaires,
            'note_globale' => $note_globale,
            'nb_stars' => $nb_stars,
            'partenaires' => $partenaires,
        ]);
    }
}
