<?php

namespace App\Controller;

use App\Entity\Commentaires;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CommentairesController extends AbstractController
{
    #[Route('/commentaires', name: 'app_commentaires')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {

        $commentaire = new Commentaires();

        $form = $this->createForm(CommentairesType::class, $commentaire);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire->setCreatedAt(new \DateTimeImmutable('now'));
            $commentaire->setIsValid(false);
            $commentaire = $form->getData();

            // Sauvegarde en BDD
            $em->persist($commentaire);
            $em->flush();

            $this->addFlash('success', 'Votre commentaire a bien été enregistré');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('commentaires/index.html.twig', [
            'form' => $form
        ]);
    }
}
