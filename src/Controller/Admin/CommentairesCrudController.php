<?php

namespace App\Controller\Admin;

use App\Entity\Commentaires;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommentairesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Commentaires::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $image = new Commentaires();
        $image->setCreatedAt(new \DateTimeImmutable('now'));

        return $image;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            
            TextField::new('pseudo'),
            TextField::new('titre'),
            IntegerField::new('note'),
            TextEditorField::new('message'),
            BooleanField::new('isValid'),
        ];
    }
    
}
