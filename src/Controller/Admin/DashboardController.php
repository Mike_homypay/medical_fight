<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Commentaires;
use App\Entity\Event;
use App\Entity\Image;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {


        return $this->render('bundles/EasyAdminBundle/layout.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('MEDICAL FIGHT');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Acceuil', 'fa fa-globe', 'app_home');
        yield MenuItem::linkToCrud('Gallerie', 'fas fa-camera', Image::class);
        yield MenuItem::linkToCrud('Evénement', 'fa-regular fa-calendar', Event::class);
        yield MenuItem::linkToCrud('Categorie', 'fa-solid fa-user-ninja', Category::class);
        yield MenuItem::linkToCrud('Commentaires', 'fa-regular fa-comments', Commentaires::class);
    }
    
}
