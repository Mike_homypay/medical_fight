<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Image::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $image = new Image();
        $image->setCreatedAt(new \DateTimeImmutable('now'));

        return $image;
    }

    public function configureFields(string $pageName): iterable
    {
        return [

            DateTimeField::new('createdAt', 'Date de l\'ajout')->onlyOnIndex(),
            TextField::new('imageFile', 'Image de présentation')->setFormType(VichImageType::class)->onlyOnForms(),
            ImageField::new('imageName', 'Miniatures')->setBasePath('/public/gallery')->onlyOnIndex(),
        ];
    }

}
