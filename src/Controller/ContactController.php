<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\RecuperationDeDonneesService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    public function __construct(
        private RecuperationDeDonneesService $recuperationDeDonnees,
        private MailerService $mailerService,
    ) {
    }

    #[Route('/contact', name: 'app_contact')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {

        $headers = $this->recuperationDeDonnees->contact()['headers'];

        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact

            ->setObjet($form->getData()->getObjet())
            ->setNom($form->getData()->getNom())
            ->setPrenom($form->getData()->getPrenom())
            ->setEmail($form->getData()->getEmail())
            ->setMessage($form->getData()->getMessage());

            $this->mailerService->sendEmail(
                $form->getData()->getEmail(),
                $form->getData()->getNom(),
                $form->getData()->getPrenom(),
                $form->getData()->getObjet(),
                $form->getData()->getMessage(),
            );

            $entityManager->persist($contact);
            $entityManager->flush();

            $this->addFlash('success', 'Votre message a bien été envoyé!');
            return $this->redirectToRoute('app_contact');
        }
        return $this->render('contact/index.html.twig', [
            'headers' => $headers,
            'form' => $form
        ]);
    }
}
