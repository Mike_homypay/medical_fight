<?php

namespace App\DataFixtures;

use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $commentaires = new \App\Entity\Commentaires();
            $commentaires->setPseudo($faker->name)
                ->setMessage($faker->text)
                ->setCreatedAt(new \DateTimeImmutable($faker->dateTimeThisYear()->format('Y-m-d H:i:s')))
                ->setTitre($faker->title)
                ->setNote($faker->numberBetween(1, 5))
                ->setValid(true);
            $manager->persist($commentaires);
        }

        // php bin/console doctrine:fixtures:load


        $manager->flush();
    }
}
