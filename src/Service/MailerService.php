<?php

// src/Controller/MailerController.php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerService extends AbstractController
{
    public function __construct(
        private MailerInterface $mailer,
    ) {
    }

    #[Route('/email')]
    public function sendEmail(
        $emailClient,
        $nom,
        $prenom,
        $objet,
        $message
    ): void {
        $contentMail = '<h1> Bonjour Pauline</h1> <p>'.$nom.', '. $prenom.' vient de vous envoyer un message</p> <h3> Objet: </h3>'.$objet.'<br><h3> voici le message: </h3> '.$message .' <h3> pour repondre: </h3> '.$emailClient;

        $email = (new Email())
            ->from($emailClient)
            ->to('contact@medical-fight.com')
            // ->cc('uber.jeff.95@gmail.com')
            // ->cc($emailClient)
            ->replyTo($emailClient)
            ->priority(Email::PRIORITY_HIGH)
            ->subject($objet)
            ->text($message)
            ->html($contentMail);

        $emailForClient = (new Email())
            ->from('contact@medical-fight.com')
            ->to($emailClient)
            // ->cc('uber.jeff.95@gmail.com')
            // ->cc($emailClient)
            ->replyTo('contact@medical-fight.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject($objet)
            ->text('')
            ->html('Le message avec le contenu suivant "'.$message.'" a bien été envoyé');

        $this->mailer->send($email);
        $this->mailer->send($emailForClient);
    }

}
