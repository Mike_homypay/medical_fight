<?php

namespace App\Service;

final class RecuperationDeDonneesService
{
    public function experienceCardItem(): array
    {

        $experience = [
            'images' => [
                [
                    'path' => '/public/images/experience/arene_violette.jpg', 'alt' => 'soin prodigué aux mains d\'un combattant'
                ],
                [
                    'path' => '/public/images/experience/pauine_explique.jpg', 'alt' => 'soin prodigué aux mains d\'un combattant'
                ],
                [
                    'path' => '/public/images/experience/pauline_a_cote_table_massage.jpg', 'alt' => 'soin prodigué aux mains d\'un combattant'
                ],
                [
                    "path" => "/public/images/experience/pauline_avec_combattant_hors_cage.jpg",
                    "alt" => "mma"
                ],
                [
                    "path" => "/public/images/experience/pauline_cage_nb.jpg",
                    "alt" => "mmb"
                ],
                [
                    "path" => "/public/images/experience/pauline_equipe_soignant_dans_cage.jpg",
                    "alt" => "mmd"
                ],
                [
                    "path" => "/public/images/experience/pauline_lit_texte.jpg",
                    "alt" => "mme"
                ],
                [
                    "path" => "/public/images/experience/pauline_osculte_machoir_nb.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/pauline_regarde_combattant_avec_qqun.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/pauline_regarde_match_nb.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/pauline_soin_dans_cage_nb.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/pauline_soin_main_nb.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/soin_combattant_sol_nb.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/soin_main_cheveux_orange.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/soin_visage.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/transport_brancard_pauline_pompier.jpg",
                    "alt" => "mmf"
                ],
                [
                    "path" => "/public/images/experience/soin_main_combattante_tresse.jpg",
                    "alt" => "mmf"
                ],
            ],
            'texts' => [
                'introduction' => [
                    $_ENV['SITE_NAME'] . " " . "a assuré la couverture médicale de <span class='font-bold'>centaines d'événements</span>, principalement en MMA professionnel au sein <span class='font-bold'>des plus grandes organisations</span> françaises ainsi qu'en MMA amateur, en boxe, en kick-boxing et en Muay Thai.",

                    "Fort d'un savoir cumulatif," . " " . $_ENV['SITE_NAME'] . " " . "a développé une <span class='font-bold'>expertise singulière</span> à la médicalisation des sports de combat et constitue ainsi, la <span class='font-bold'>référence française</span> dans ce domaine.",
                ]

            ]
        ];

        return $experience;
    }

    public function experience(): array
    {

        $confiance = [
            'images' => [
                [
                    'path' => 'public/images/confiance/IMG-20240418-WA0018.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                [
                    'path' => 'public/images/confiance/IMG-20240418-WA0019.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                /*   [
                    'path' => 'public/images/confiance/IMG-20240418-WA0020.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME']
                ], */
                [
                    'path' => 'public/images/confiance/Screenshot_20240409_034441_Instagram.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                [
                    'path' => 'public/images/confiance/Polish_20230603_210120498.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                [
                    'path' => 'public/images/confiance/Polish_20230603_210608642.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ], 
                [
                    'path' => 'public/images/confiance/Polish_20230603_210806881.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                [
                    'path' => 'public/images/logo/Medical_Fight_whit&gold.png', 'alt' => 'Logo ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                [
                    'path' => 'public/images/confiance/Polish_20230603_210844028.jpg', 'alt' => 'Témoignagne de confiance au profit de ' . ' ' . $_ENV['SITE_NAME'], 'class' => ''
                ],
                [
                    'path' => 'public/images/logo/emblemeMedicalFightWhite&gold.png', 'alt' => 'Logo ' . ' ' . $_ENV['SITE_NAME'], 'class' => 'md:block hidden'
                ],
            ]
        ];

        return $confiance;
    }

    public function presentation(): array
    {
        $presentation  = [
            'title_presentation' => 'Témoignage de confiance au profit de ' . ' ' . $_ENV['SITE_NAME']
        ];

        return $presentation;
    }

    public function partenaires(): array
    {
        $partenaires = [
            'images' => [
                [
                    'asset' => 'public/images/partenaire/ares.png',
                    'alt' => 'Ares figting championship'
                ],
                [
                    'asset' => 'public/images/partenaire/brave.png',
                    'alt' => 'Brave combat federation'
                ],
                [
                    'asset' => 'public/images/partenaire/hexagone_mma.png',
                    'alt' => 'Hexagone MMA'
                ],
                [
                    'asset' => 'public/images/partenaire/kof.png',
                    'alt' => 'The king of fighters'
                ],
                [
                    'asset' => 'public/images/partenaire/pef.png',
                    'alt' => 'Pro evolution fighting'
                ],
                [
                    'asset' => 'public/images/partenaire/100_fight.png',
                    'alt' => '100% fight'
                ],
                [
                    'asset' => 'public/images/partenaire/ljf_championship.png',
                    'alt' => 'LJF championship'
                ],

            ]
        ];
        return $partenaires;
    }

    public function contact()
    {
        $headers = [
            [
                'font' => 'location-dot', 'text' => '7, Impasse des chasses marées 95610 Éragny, France'
            ],  [
                'font' => 'envelope-open-text', 'text' => 'contact@karaisone.fr'
            ],  [
                'font' => 'phone', 'text' => '+33 1 83 64 44 67</br>+33 6 09 99 32 08'
            ],
        ];

        return ['headers' => $headers];
    }
}
